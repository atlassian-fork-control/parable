const babel = require('@babel/core');
const fs = require('fs-extra');
const parable = require('parable');

const defaultOptions = {
    babelOptions: {},
    output: parable.Output.STRING,
    skipOutputDirCheck: true,
};

const createParablePreprocessor = function (args, config, logger, helper) {
    const log = logger.create('preprocessor.parable');
    config = Object.assign({}, defaultOptions, config);
    if (config.output === parable.Output.FILE && config.outputDir) {
        log.debug('Creating directory %s', config.outputDir);
        fs.mkdirpSync(config.outputDir);
    }
    return (content, file, done) => {
        log.debug('Processing "%s".', file.originalPath);

        // this will return the cached file if it had already been transpiled with the same source, else it'll transpile
        // and cache the result for next time. win-win-win.
        parable.transpile(file.originalPath, config)
            .then(transpiled => {
                if (config.filenameMapper) {
                    file.path = config.filenameMapper(file);
                    log.debug('Remapped %s to %s', file.originalPath, file.path);
                }

                if (transpiled.code) {
                    log.debug('Returning string for %s', file.originalPath);
                    return done(null, transpiled.code);
                 } else {
                    log.debug('Returning %s for %s', transpiled.output, file.originalPath);
                    return done(null, fs.readFileSync(transpiled.output, 'utf8'));
                }
            })
            .catch(err => {
                log.error(err);
                return done(err)
            });
    }
};

createParablePreprocessor.$inject = ['args', 'config.parablePreprocessor', 'logger', 'helper'];

module.exports = {
    'preprocessor:parable': ['factory', createParablePreprocessor]
};