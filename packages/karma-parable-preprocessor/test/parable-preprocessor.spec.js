'use strict';

const expect = require('chai').expect;
const fs = require('fs-extra');
const path = require('path');
const preprocessor = require('../index');
const tmp = require('tmp');
const parable = require('parable');

const factory = preprocessor['preprocessor:parable'][1];

const mockLogger = {
    create: () => {
        return {
            debug: () => console.log.apply(console, arguments),
            error: () => console.error.apply(console, arguments)
        }
    }
};

const config = {
    babelOptions: {
        presets: [
            [
                '@babel/preset-env',
                {
                    targets: {
                        ie: 11,
                        browsers: 'last 2 versions',
                    },
                },
            ],
        ]
    },
    output: parable.Output.FILE,
    outputDir: tmp.dirSync().name,
    base: path.resolve('test/fixtures')
};

const outputDir = path.resolve('test/fixtures/output');

describe('karma-parable-preprocessor', () => {
    beforeEach(function () {
        const src = path.resolve('test/fixtures/source.js');

        return parable._processor._readSourceFile(src, config.babelOptions)
            .then(file => {
                fs.mkdirpSync(outputDir);
                fs.writeFileSync(path.join(outputDir, file.hash + '.js'), 'CODE_MARKER');
            });
    });

    afterEach(function () {
        fs.removeSync(outputDir);
    });

    it('returns the cached file if it was transpiled', (done) => {
        const instance = factory([],
            Object.assign({}, config, {
                outputDir: path.resolve('test/fixtures/output')
            }), mockLogger);

        instance('', {
            originalPath: path.resolve('test/fixtures/source.js')
        }, (err, contents) => {
            if (err) {
                return done(err);
            }
            expect(contents).to.equal('CODE_MARKER');
            done();
        });
    });

    it('transpiles if the file was not cached', (done) => {
        const instance = factory([], config, mockLogger);
        instance('', {
            originalPath: path.resolve('test/fixtures/source.js')
        }, (err, contents) => {
            if (err) {
                return done(err);
            }
            expect(contents).to.have.length.above(0);
            done();
        });
    });

    it('transpiles to memory if output is STRING not specified', (done) => {
        const instance = factory([],
            Object.assign({}, config, {
                output: parable.Output.STRING,
            }), mockLogger);

        instance('', {
            originalPath: path.resolve('test/fixtures/source.js')
        }, (err, contents) => {
            if (err) {
                return done(err);
            }
            expect(contents).to.have.length.above(0);
            expect(contents[0]).to.not.equal('/'); // not a path
            done();
        })
    })
});