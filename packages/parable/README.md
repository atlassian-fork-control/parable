# Parable - Parallel Babel Runner

This is a module that makes use of Node's `cluster` module to run Babel transpilation in parallel. In addition it
provides the ability to cache results on disk to speed up subsequent rebuilds.

This module, and it's documentation, is still under active development and will be likely be changing often.

Current implementations for other build related tools using Parable are:

 * [gulp-parable](https://www.npmjs.com/package/gulp-parable)
 * [karma-parable-preprocessor](https://www.npmjs.com/package/karma-parable-preprocessor)

Get in touch if you add one we can add to the list!

## Usage

### Basic usage as a module

```js
const parable = require('parable');

parable.transformFiles(glob.sync('**/*.js'), {})
    .then(files => {
        console.log(files);
    });
```

#### Options

Default options:

```js
{
    babelOptions: {},
    logLevel: 'error',
    scale: SCALE,
    processes: null,
    output: Output.STRING,
    outputDir: null,
    base: '',
    key: ''
}
```

##### babelOptions

The options to pass to the Babel transpiler (note that `.babelrc` files will also be read as normal). These are mostly
passed as-is to Babel, so refer to [Babel's options](https://babeljs.io/docs/usage/options/) for documentation, with one
exception - the two options that are a function - `getModuleId` and `shouldPrintComment`.

Since we pass options to the workers from the master using stringified JSON we can't pass a function, instead these options
should be passed as a string which is the name of a module to require which will provide a function to be called. For
 example:

```js
parable.transformFiles(glob.sync('**/*.js'), {
    babelOptions: {
        getModuleId: path.resolve('./babel-get-module-id')
    }
});
```

```js
// babel-get-module-id.js
module.exports = (moduleName) => moduleName.toUpperCase(); // OK?!
```

##### logLevel

Level of logging to use - one of the usual suspects, `error`, `warn`, `info`, `debug`.

##### scale

Scale factor to apply to number of CPUs to determine how many parallel processes to run - default is 0.5
(ie. 4 processes for 8 cores). See also `processes` if you want to set an explicit number of processes.

##### processes

Number of parallel processes to run - default is number of cores / `scale`.

##### output

By default the workers will return the transpiled code, set this to `parable.Output.FILE` to write to a file instead. If you want
 to take advantage of caching between runs you will need to write to a file.

##### outputDir

The directory to write files to when the `output` is `parable.Output.FILE`. Defaults to a system temporary directory created using the
`tmp` module.

###### base

The base directory to pass into Babel. This is used to calculate relative filenames, for example when naming AMD modules using Babel's
`moduleIds` option.

This can be specified as a single string, or an array of base paths - if an array is supplied then the closest matching base path for
the path being transpiled will be chosen. If no match is found, the path will be relative to the first entry in the base array.

###### key

Default is an empty string. A key for this config. As the config object is used when generating the hash, this is to ensure
unique hashes if the only config differences are in `.babelrc` files.