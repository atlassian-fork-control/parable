const SCALE = 0.5;
const Output = {
    STRING: 'STRING',
    FILE: 'FILE'
};

module.exports = {
    Output,
    defaultOptions: {
        babelOptions: {},
        logLevel: 'error',
        scale: SCALE,
        processes: null,
        output: Output.STRING,
        outputDir: null,
        base: '',
        key: '',
        env: {}
    }
};
