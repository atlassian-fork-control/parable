'use strict';

const babel = require('@babel/core');
const fs = require('fs-extra');
const crypto = require('crypto');
const path = require('path');
const winston = require('winston');

const config = require('./config');
const ParableError = require('./error');

const BABEL_CALLBACK_OPTIONS = ['getModuleId', 'resolveModuleSource', 'shouldPrintComment'];

function targetFilename(options, hash) {
    return path.join(options.outputDir, hash) + '.js';
}

function readSourceFile(filename, options) {
    return new Promise(resolve => {
        const hash = crypto.createHash('md5');
        const input = fs.createReadStream(filename);
        let contents = "";

        hash.setEncoding('hex');
        hash.update(filename);
        hash.update(JSON.stringify(options || {}));
        hash.update(process.env.NODE_ENV || '');
        hash.update(process.env.BABEL_ENV || '');

        input.on('end', () => {
            hash.end();
            const hashDigest = hash.read();
            winston.debug(`Read ${contents.length} bytes from ${filename} => ${hashDigest}`);
            resolve({
                contents,
                hash: hashDigest
            });
        });
        input.on('data', chunk => {
            contents += chunk;
        });

        input.pipe(hash);
    });
}

function findExisting(hash, options) {
    if (options && options.output === config.Output.STRING) {
        return null;
    }

    const target = targetFilename(options, hash);
    try {
        const result = {};
        const existing = fs.readFileSync(target, 'utf8');

        try {
            result.map = fs.readFileSync(target + '.map', 'utf8');
            result.outputMap = target + '.map';
        } catch (e) {}

        result.code = existing;
        result.fromCache = true;
        result.output = target;
        return result;
    } catch (e) {
        return null;
    }
}

function _resolveRelative(filename, basePaths) {
    const basePath = basePaths.reduce((memo, path) =>
        filename.startsWith(path) &&
        (!memo || path.length > memo.length) ? path : memo, null);

    return path.relative(basePath || basePaths[0], filename)
}

function transform(filename, contents, hash, options) {
    return new Promise((resolve, reject) => {
        const result = findExisting(hash, options);

        if (result) {
            return resolve(result);
        }

        try {
            const transformOptions = Object.assign({
                filename,
                filenameRelative: _resolveRelative(filename, options.base)
            }, options.babelOptions);

            BABEL_CALLBACK_OPTIONS.filter((callback) => transformOptions.hasOwnProperty(callback)
                && typeof transformOptions[callback] === 'string')
                .forEach((callback) => {
                    transformOptions[callback] = require(transformOptions[callback]);
                });

            const transformed = babel.transformSync(contents, transformOptions);

            resolve({
                code: transformed.code,
                map: transformed.map,
                fromCache: false,
                _transformOptions: transformOptions,
            });
        } catch (e) {
            reject(e);
        }
    });
}

function updateCache(result, options) {
    if (options.output === config.Output.STRING || result.fromCache) {
        return result;
    }

    const target = targetFilename(options, result.hash);
    fs.writeFileSync(target, result.code);
    result.output = target;

    const mapTarget = target + '.map';

    if (result.map) {
        fs.writeFileSync(mapTarget, JSON.stringify(result.map));
        result.outputMap = mapTarget;
    } else {
        result.outputMap = null;
    }

    return result;
}

function transpile(source, options) {
    const result = {
        source
    };
    return new Promise((resolve, reject) => {
        readSourceFile(source, options && options.babelOptions || {})
            .then(input => {
                result.hash = input.hash;
                return transform(source, input.contents, input.hash, options);
            })
            .then(transformed => Object.assign({}, result, transformed))
            .then(transformed => updateCache(transformed, options))
            .then(transformed => {
                resolve(transformed);
            })
            .catch(error => {
                reject(new ParableError(error, source));
            });
    });
}

module.exports = {
    transpile,
    targetFilename: targetFilename,
    _readSourceFile: readSourceFile,
    _transform: transform,
};