'use strict';

const expect = require('chai').expect;
const fs = require('fs-extra');
const parable = require('../lib/parable');
const path = require('path');
const tmp = require('tmp');

const fixturePath = fixture => path.resolve(`./test/fixtures/${fixture}.js`);
const basicOpts = {
    babelOptions: {
        sourceMaps: false,
        presets: [
            [
                '@babel/preset-env',
                {
                    targets: {
                        ie: 11,
                        browsers: 'last 2 versions',
                    },
                },
            ]
        ],
    },
    logLevel: 'error'
};

const basicOptsWithSourcemaps = Object.assign({}, basicOpts, {
    babelOptions: {
        sourceMaps: 'both'
    }
});

const outputDir = path.resolve('./test/fixtures/output');

function transpileFixtures(opts) {
    const src = path.resolve('test/fixtures/source.js');
    return parable._processor._readSourceFile(src, opts || basicOptsWithSourcemaps.babelOptions)
        .then(file => {
            fs.mkdirpSync(outputDir);
            fs.writeFileSync(path.join(outputDir, file.hash + '.js'), 'CODE_MARKER');
            fs.writeFileSync(path.join(outputDir, file.hash + '.js.map'), 'MAP_MARKER');
        });
}

describe('parable', function () {
    this.timeout(10000); // Pipelines is sometimes slow?

    beforeEach(function () {
        return transpileFixtures();
    });

    afterEach(function () {
        fs.removeSync(outputDir);
    });

    it('does a basic transpile to memory without sourcemaps', () => {
        const srcPath = fixturePath('source');
        const src = fs.readFileSync(srcPath, 'utf8');
        return parable.transformFiles([
            srcPath
        ], basicOpts).then(files => {
            expect(files).to.have.length(1);
            expect(files[0].code).not.to.equal(src);
            expect(files[0].code).to.have.length.above(0);
            expect(files[0].map).to.be.null;
        });
    });

    it('does a basic transpile to memory with sourcemaps', () => {
        const srcPath = fixturePath('source');
        const src = fs.readFileSync(srcPath, 'utf8');
        return parable.transformFiles([
            srcPath
        ], basicOptsWithSourcemaps).then(files => {
            expect(files).to.have.length(1);
            expect(files[0].code).not.to.equal(src);
            expect(files[0].code).to.have.length.above(0);
            expect(files[0].map).to.not.be.null;
        });
    });

    it('fails with bad sources', (done) => {
        parable.transformFiles([fixturePath('bad-source')], basicOpts)
            .then(files => done('did not expect promise to resolve'))
            .catch(errors => {
                expect(errors).to.have.length(1);
                done();
            })
    });

    it('does a basic transpile to file without sourcemaps', () => {
        const tmpDir = tmp.dirSync();
        return parable.transformFiles([fixturePath('source')], Object.assign({}, basicOpts,
            {
                output: parable.Output.FILE,
                outputDir: tmpDir.name
            }))
            .then(files => {
                expect(files).to.have.length(1);
                expect(files[0].code).to.be.null;
                expect(files[0].map).to.be.null;
                const target = fs.readFileSync(files[0].output);
                expect(target).to.have.length.above(0);
                expect(files[0].outputMap).to.be.null;
            });
    });

    it('does a basic transpile to file with sourcemaps', () => {
        const tmpDir = tmp.dirSync();
        return parable.transformFiles([fixturePath('source')], Object.assign({}, basicOptsWithSourcemaps,
            {
                output: parable.Output.FILE,
                outputDir: tmpDir.name
            }))
            .then(files => {
                expect(files).to.have.length(1);
                expect(files[0].code).to.be.null;
                expect(files[0].map).to.be.null;
                const target = fs.readFileSync(files[0].output);
                expect(target).to.have.length.above(0);
                const sourceMap = fs.readFileSync(files[0].outputMap);
                expect(sourceMap).to.have.length.above(0);
            });
    });

    it('re-uses an existing file if it exists', () => {
        return parable.transformFiles([fixturePath('source')], Object.assign({}, basicOptsWithSourcemaps,
            {
                output: parable.Output.FILE,
                outputDir: outputDir,
            }))
            .then(files => {
                expect(files).to.have.length(1);
                const code = fs.readFileSync(files[0].output, 'utf8');
                const sourceMap = fs.readFileSync(files[0].outputMap, 'utf8');
                expect(code).to.equal('CODE_MARKER');
                expect(sourceMap).to.equal('MAP_MARKER');
            })
    });

    it('successfully spins up multiple processes and joins results', function() {
        const files = [];
        for(let i = 0; i < 1000; i++) {
            files.push(fixturePath('source'));
        }
        return parable.transformFiles(files, Object.assign({}, basicOpts, {
            processes: 2
        }))
            .then(files => {
                expect(files).to.have.length(1000);
            });
    });

    it('can pass code as options', function () {
        const opts = Object.assign({}, basicOpts, {
            babelOptions: {
                plugins: [
                    '@babel/plugin-transform-modules-amd'
                ],
                moduleIds: true,
                getModuleId: path.resolve('./test/fixtures/get-module-ids')
            }
        });
        return parable.transformFiles([fixturePath('source')], opts)
            .then(files => {
                expect(files).to.have.length(1);
                expect(files[0].code).to.contain('YOLO');
            });
    });

    it('uses the filename as part of the hash', function () {
        // if the filename isn't part of the hash then this will return the same file is produced from fixtures/source
        return parable.transformFiles([fixturePath('other-path/source')], Object.assign({}, basicOptsWithSourcemaps, {
            output: parable.Output.FILE,
            outputDir: outputDir,
        }))
            .then(files => {
                expect(files).to.have.length(1);
                const code = fs.readFileSync(files[0].output, 'utf8');
                expect(code).to.not.equal('CODE_MARKER'); // this is in the "cached" file source.js
                const map = fs.readFileSync(files[0].outputMap, 'utf8');
                expect(map).to.not.equal('MAP_MARKER'); // this is in the "cached" file source.js
            });
    });


    describe('environment tests', () => {
        let babelEnv;
        let nodeEnv;
        beforeEach(() => {
            babelEnv = process.env.BABEL_ENV;
            nodeEnv = process.env.NODE_ENV;
        });
        afterEach(() => {
            process.env.BABEL_ENV = babelEnv;
            process.env.NODE_ENV = nodeEnv;
        });
        it('uses NODE_ENV and BABEL_ENV as part of the hash', function () {
            const srcPath = fixturePath('source');
            const defaultEnv = parable.transformFiles([
                srcPath
            ], basicOptsWithSourcemaps);

            process.env.BABEL_ENV = 'foo';
            process.env.NODE_ENV = 'bar';

            const updatedEnv = parable.transformFiles([
                srcPath
            ], basicOptsWithSourcemaps);

            return Promise.all([defaultEnv, updatedEnv]).then(function (results) {
                expect(results[0][0].hash).not.to.equal(results[1][0].hash);
            })
        });
    });

    it('can transpile a single file', function () {
        const outputSubDir = path.join(outputDir, 'subdir');
        return parable.transpile(fixturePath('source'), Object.assign({},
            basicOpts, {
                base: path.resolve('./test/fixtures'),
                output: parable.Output.FILE,
                outputDir: outputSubDir
            }
        )).then(file => {
            expect(fs.readFileSync(file.output, 'utf8')).to.have.length;
        });
    });
});