const config = require('../lib/config');
const processor = require('../lib/processor');
const expect = require('chai').expect;

describe('processor', function () {
    describe('_transform', function () {
        it('generates a simple relative path', function () {
            const options = {
                output: config.Output.STRING,
                base: ['/a']
            };
            return processor._transform('/a/b', '', null, options)
                .then(result => {
                    expect(result._transformOptions.filenameRelative).to.equal('b');
                });
        });

        it('supports multiple base paths', function () {
            const options = {
                output: config.Output.STRING,
                base: ['/a', '/c/d/e', '/c'] // order is important - we want to test that "best" path is chosen
            };

            function transform(filename) {
                return processor._transform(filename, '', null, options)
            }

            const testCases = {
                '/a/b': 'b',
                '/c/d/x/y/z': 'd/x/y/z',
                '/c/d/e/f/g': 'f/g'
            };

            const promises = Object.keys(testCases).map(filename => transform(filename));

            return Promise.all(promises)
                .then(results => {
                    results.forEach(result => {
                        const filename = result._transformOptions.filename;
                        expect(result._transformOptions.filenameRelative).to.equal(testCases[filename]);
                    });
                });
        });

        it(`uses first path when base path can't be resolved`, function () {
            const options = {
                output: config.Output.STRING,
                base: ['/a/b', '/c']
            };

            return processor._transform('/x/y/z', '', null, options)
                .then(result => {
                    expect(result._transformOptions.filenameRelative).to.equal('../../x/y/z')
                });
        });
    });
});